# comp3770-project

## Overview

This project was developed as part of the Comp-3770 Game Design, Development, and Tools course at the University of Windsor. The game was designed and developed collaboratively by a team of students, utilizing Unity for game development and GitLab for version control and collaboration. After the University of Windsor's GitLab instance was shut down, the project was transferred to here.

## Team Members

- **Justin Neal** - Level Architect, Level Designer, Gameplay Programmer  
  [Connect with Justin on LinkedIn](https://www.linkedin.com/in/justin-neal-68985a250/)
- **Bailey Chittle** - Networking, Lead Gameplay Programmer  
  [Connect with Bailey on LinkedIn](https://www.linkedin.com/in/bailey-chittle/)
- **Veronica (Nonika) Reingold** - UI/UX Designer, Gameplay Programmer  
  [Connect with Nonika on LinkedIn](https://www.linkedin.com/in/veronicareingold/)


## Project Description

**Capture The Flegg** is an exciting first-person shooter (FPS) game set on a farm where players control chickens in two teams: red and blue. The objective is to capture the opponent's flag (flegg) and return it to your base while defending your own. The game combines fast-paced action with strategic elements, requiring teamwork and coordination.

## Features

- **Gameplay Mechanics:** Players can run, jump, and shoot to capture the opponent's flegg and defend their own.
- **Levels:** The game features a detailed farm environment with various obstacles and hiding spots.
- **Characters:** Play as either a red or blue chicken, each with unique abilities and weapons.
- **Audio:** Original background music and sound effects enhance the immersive farm experience.
- **Graphics:** Vibrant and engaging graphics depicting a whimsical farm setting.

## Development Process

This project was developed through iterative sprints, with regular team meetings and code reviews. Each team member contributed to different aspects of the game, ensuring a collaborative and well-rounded development process.

## Gameplay Screenshots
![Load Screen](./exe/img/LoadScreen.png)
![Gameplay Screenshot](./exe/img/GamePlay.png)


## Acknowledgements

We would like to thank our instructor, Dr. Scott Goodwin [R.I.P], and the University of Windsor for providing the resources and support needed for this project.
